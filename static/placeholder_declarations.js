
var PlaceHolder = function ($scope, $element, $http, $timeout, $compile) {
    return [];
};
const templateTypeName = 'Shipping Labels';
const redirectTypeName = 'Invoice Template';
const redirectTemplateId = 32;

const ignoreCountries = [
    'Austria',
    'Belgium',
    'Bulgaria',
    'Croatia',
    'Cyprus',
    'Czechia',
    'Denmark',
    'Estonia',
    'Finland',
    'France',
    'Germany',
    'Greece',
    'Hungary',
    'Ireland',
    'Italy',
    'Latvia',
    'Lithuania',
    'Luxembourg',
    'Malta',
    'Netherlands',
    'Poland',
    'Portugal',
    'Romania',
    'Slovakia',
    'Slovenia',
    'Spain',
    'Sweden',
    'United Kingdom',
];

const temp = Services.PrintService.prototype.CreatePDFfromJobForceTemplate;
Services.PrintService.prototype.CreatePDFfromJobForceTemplate = function (templateType, IDs, templateID, parameters, printerName, callback, printZone) {
    const printService = this;
    if (templateType === templateTypeName) {
        const service = new Services.OrdersService(printService.options);
            service.GetOrdersById(IDs, function(event) {
                if (event && event.hasErrors() == false) {
                    const orders = event.result;

                    const ordersWithDeclaration = orders.filter(o => {
                        const country = o.CustomerInfo && o.CustomerInfo.Address && o.CustomerInfo.Address.Country;

                        return !ignoreCountries.some(c => c === country);
                    });

                    const ordersWithDeclarationIds = ordersWithDeclaration
                        .map(o => o.OrderId);

                    if (ordersWithDeclarationIds.some(c => true)) {
                        const newCallback = function(e, a, b, c) {
                            callback(e, a, b, c);
                            Services.executeRequestBySession(
                                printService.options,
                                printService.root + "CreatePDFfromJobForceTemplate", {
                                    templateType: redirectTypeName,
                                    IDs: ordersWithDeclarationIds,
                                    templateID: redirectTemplateId,
                                    parameters: parameters,
                                    printerName: printerName,
                                    printZoneCode: printZone
                                },
                                callback
                            );
                        }
                        temp.call(printService, templateType, IDs, templateID, parameters, printerName, newCallback, printZone);
                    }
                    else {
                        temp.call(printService, templateType, IDs, templateID, parameters, printerName, callback, printZone);
                    }
                }
            });
    }
    else {
        temp.call(printService, templateType, IDs, templateID, parameters, printerName, callback, printZone);
    }
}

Core.PlaceHolderManager.register("OpenOrders_ProcessOrders_RightBottomButtons", PlaceHolder);